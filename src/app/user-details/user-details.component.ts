import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ListService } from '../list.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  constructor(private listService:ListService,private router:Router) { }

  id:number;
  Title:string;
  ToDoId:number;
  b:any = [];
  user:any;

  ngOnInit(): void {
    this.PopulateDetails()
  }

  PopulateDetails()
  {
    this.id = this.listService.getId()
    this.Title = this.listService.getTitle()
    this.ToDoId = this.listService.gettoDoId();
    console.log("id Is : " + this.id)
    console.log("Title Is : " + this.Title)
    console.log("ToDoId Is : " + this.ToDoId)  
    this.showDeets();
  }

  showDeets()
  {
    this.listService.OnShowDeets(this.id)
    .subscribe(response => 
      {
      console.log("Response for user details is : " + response)
      this.b = JSON.stringify(response)
      this.b = JSON.parse(this.b)
      
      // this.user = this.b.filter((item) => {
      //   return item
      // })


      console.log("User Details " + this.b)
    })
  }

}
