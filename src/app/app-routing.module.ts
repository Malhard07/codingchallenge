import { UserDetailsComponent } from './user-details/user-details.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ToDoListComponent } from './to-do-list/to-do-list.component';

const routes: Routes = [{path: 'todolist', component: ToDoListComponent},
{path: 'userDetails', component: UserDetailsComponent},
{path: '', pathMatch: 'full', redirectTo: '/todolist'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
