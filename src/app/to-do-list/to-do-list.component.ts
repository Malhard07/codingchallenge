import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ListService } from '../list.service';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {

  list = [];
  a : any = [];
  Id: number;
  user:any;
  b:any;
  Todoid:number;
  Title:string;
  searchText = '';
  titleSearch = ''
  selectedRadioValue: any
  lang;
  headers;
  DoSort:number ;
  showView:number = 0;
  bb:any = []

    constructor(private listService:ListService,private router:Router)
    {  }
  ngOnInit(): void 
  {
    this.lang = localStorage.getItem('lang') || 'en-US'

     this.headers = new HttpHeaders({
      'Accept-Language':this.lang,
      'Content-Language' : this.lang
    })
    this.ToDoList();
  }

  onTitleFilter()
  {
    this.titleSearch = this.searchText;
  }

  onTitleFilterClear()  
  {
    this.searchText = '';
    this.titleSearch = '';
  }

  ToDoList()
  {
    console.log("In ToDoList Function")
    this.listService.OnGetToDoList(this.headers)
    .subscribe(response => 
      {
      this.a = JSON.stringify(response);
      this.a = JSON.parse(this.a)
      console.log("Response is : " + this.a)

      // Below line sorts in descending order according to Id
        if(this.DoSort === 0)
        {
          this.a.sort((a,b) => 0 - (a > b ? -1 : 1));
        }
      
      this.list = this.a.filter((item) => {
        return item
      })

      })
  }

  ToDoSort()
  {
    if(this.DoSort === 0)
    {
      this.DoSort = 1;
    }

    else
    {
      this.DoSort = 0;
    }

    this.ToDoList();
    window.location.reload();
  }

  showDetails(id, todoId, Title)
  {
    if(this.showView === 0)
    {
      this.showView = 1;
    }

      else
      {
        this.showView = 0;
      }
      
    
    
    this.Id = id
    this.Todoid = todoId;
    this.Title = Title;
    console.log("User Id Is " + this.Id)
    
    this.listService.OnShowDetails(this.Id, this.Todoid, this.Title)
    .subscribe(response => 
      {
        
        this.bb = JSON.stringify(response)
      this.bb = JSON.parse(this.bb)
      console.log("Employee Details are : " + this.bb)
    })
  }


}
