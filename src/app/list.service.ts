import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  id:number;
  toDoId:number;
  title:string;
  
  constructor(private router:Router,
    private httpClient:HttpClient) { }

  OnGetToDoList(headers)
  {
    return this.httpClient.get("https://jsonplaceholder.typicode.com/todos",{
      headers:headers
    })
  }

  
  OnShowDetails(id,toDoId,title)
  {
    this.id = id;
    this.toDoId = toDoId;
    this.title = title;
    return this.httpClient.get("https://jsonplaceholder.typicode.com/users/" + id)
  }

  OnShowDeets(id)
  {
    return this.httpClient.get("https://jsonplaceholder.typicode.com/users/" + id)
  }

  getId()
  {
    return this.id
  }

  gettoDoId()
  {
    return this.toDoId
  }

  getTitle()
  {
    return this.title
  }
}
