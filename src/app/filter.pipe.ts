import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform 
{

  transform(items: any[], searchText: any, propName: string): any[] 
  {
    
    const resultArray = []

    if(items.length === 0 || searchText === '' || propName === '')
    {
      return items;
    }

    for(let item of items)
    {
      console.log("In filter pipe")
      if(item[propName] == searchText)
      {
        // item = JSON.stringify(item)
        // item = JSON.parse(item);
        console.log("Pushed Item is : " + item)
        resultArray.push(item);
      }
    }

    return resultArray;

  }
}