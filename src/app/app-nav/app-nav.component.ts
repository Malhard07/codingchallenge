import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-nav',
  templateUrl: './app-nav.component.html',
  styleUrls: ['./app-nav.component.css']
})
export class AppNavComponent implements OnInit {

  lang;
  constructor() { }

  ngOnInit(): void {
    this.lang = localStorage.getItem('lang') || 'en-US';
  }

  changeLang(lang)
  {
localStorage.setItem ('lang', lang)
window.location.reload();
}
}
