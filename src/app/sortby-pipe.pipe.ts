import { Pipe, PipeTransform } from '@angular/core';
import { orderBy } from 'lodash';

@Pipe({
  name: 'sortbyPipe'
})
export class SortbyPipePipe implements PipeTransform 
{
  arr:any[]
  str: string
  transform(value: Array<string>, args : any[]): any 
  {
    
    const sortField = args[0];
    const sortDirection = args[1];
    let multiplier = 1;
     value as unknown;  
    //  JSON.stringify(value)
    //   JSON.parse(value)
    this.arr = value
  
    this.str = JSON.stringify(this.arr)
    value = JSON.parse(this.str)
    value = value.filter((item) => {
      return item
    })
    console.log("value of string is : " + this.str)
    console.log("In Sort Pipe")
    console.log("Received array is : " + value)

    if(sortDirection === 'desc')
    {
      multiplier = -1;
    }

    value.sort((a: any, b: any) => {
      if(a[sortField] < b[sortField])
      {
        return -1 * multiplier;
      }

      else if(a[sortField] > b[sortField])
      {
        return 1 * multiplier;
      }

      else
      {
        return 0;
      }

    })
  }

}
